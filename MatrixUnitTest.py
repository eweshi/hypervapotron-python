#!/usr/bin/env python

import xlwt
from DomainExpressions import DomainExpressions


class MatrixUnitTest(object):
    """This class provides unit testing functions and options for testing the matrix. It
    also provides visualization functions"""

    def __init__(self):

        self.wb, self.ws = self.xlFactory()
        self.testobj = DomainExpressions()
        self.populateCoefficientMatrix()



    def xlFactory(self):
            """This function makes an xl object and gives it some properties. Returns xl worksheet and workbook object"""

            font0 = xlwt.Font()
            font0.name = 'Arial'
            font0.colour_index = 0
            font0.bold = True
            font0.underline = True

            style0 = xlwt.XFStyle()
            style0.font = font0
            style1 = xlwt.XFStyle()

            wb = xlwt.Workbook()
            ws = wb.add_sheet('CoefficientMatrixVisualization')

            ws.write(0, 13, "Coefficient Matrix Visualization", style0)

            return wb, ws

    def populateCoefficientMatrix(self):
        self.testobj.dummySolve()

    def setConvergenceNum(self, num):
        self.testobj.setConvergeNum(num)


    def printCoeffMat(self):

        self.wb = xlwt.Workbook()
        self.ws = self.wb.add_sheet('CoefficientMatrixVisualization')

        self.ws.write(0, 13, "Coefficient Matrix Visualization",)

        rowstart = 5
        colstart = 3
        self.populateCoefficientMatrix()

        #Print Values
        for i in range(0, self.testobj.convergenceNum*8):
            for k in range(0, self.testobj.convergenceNum*8):
                self.ws.write(rowstart + i, colstart + k, self.testobj.coeffMat[i, k])

        rowstart = 1
        colstart = 3
        #Print Computer Index
        for i in range(0, self.testobj.convergenceNum*8):
            self.ws.write(rowstart, colstart+i, i)

        rowstart = 2
        colstart = 3
        #Print Mathematical Index
        for i in range(0, self.testobj.convergenceNum*8):
            self.ws.write(rowstart, colstart+i, i+1)


        rowstart = 5
        colstart = 0
        #Print Computer Index
        for i in range(0, self.testobj.convergenceNum*8):
            self.ws.write(rowstart+i, colstart, i)

        rowstart = 5
        colstart = 1
        #Print Mathematical Index
        for i in range(0, self.testobj.convergenceNum*8):
            self.ws.write(rowstart+i, colstart, i+1)


        self.wb.save('coeffmatvis.xls')

        return False







a = MatrixUnitTest()
a.printCoeffMat()

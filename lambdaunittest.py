#!/usr/bin/env python

from DomainExpressions import DomainExpressions
import xlwt



class lambdaunittest(object):
    """This class is a unit testing class for testing the lambda list integrity
    within the Domain Expressions class"""

    font0 = xlwt.Font()
    font0.name = 'Arial'
    font0.colour_index = 0
    font0.bold = True
    font0.underline = True

    style0 = xlwt.XFStyle()
    style0.alignment = xlwt.Alignment.HORZ_CENTER
    style0.font = font0
    style1 = xlwt.XFStyle()

    wb = xlwt.Workbook()
    ws = wb.add_sheet('Eigen-values')

    ws.write(3, 0, "Bi = 3", style0)
    rowstart = 3


    def __init__(self):
        self.testobj = DomainExpressions()


    def startTest(self):

        font0 = xlwt.Font()
        font0.name = 'Arial'
        font0.colour_index = 0
        font0.bold = True
        font0.underline = True

        style0 = xlwt.XFStyle()
        #style0.alignment = xlwt.Alignment.HORZ_CENTER
        style0.font = font0
        style1 = xlwt.XFStyle()

        wb = xlwt.Workbook()
        ws = wb.add_sheet('Eigen-values')

        ws.write(2, 0, "Bi = 3", style0)
        rowstart = 3
        rowindex = 0
        colstart = 0

        print "Starting Lambda Unit Test"
        self.testobj.computelams()

        for i, v in enumerate(self.testobj.lambdaList):
            print i, v
            for j,k in enumerate(v):
                ws.write(rowstart+j, colstart, format(k,"0.4f"))

            colstart += 1


        wb.save('simple3.xls')


    def setConvNum(self, x):
        self.testobj.setConvergeNum(x)

    def printParams(self):
        print "H_1: {0}".format(self.testobj.H_1)
        print "H: {0}".format(self.testobj.height)
        print "H_c: {0}".format(self.testobj.height_c)
        print "t: {0}".format(self.testobj.thickness)
        print "x_1: {0}".format(self.testobj.x_1)
        print "width: {0}".format(self.testobj.width)
        print "width_c: {0}".format(self.testobj.width_c)
        print "Bi_1: {0}".format(self.testobj.Bi_1)
        print "Bi_2: {0}".format(self.testobj.Bi_2)
        print "Bi_3: {0}".format(self.testobj.Bi_3)

    def printMat(self):

        wb = xlwt.Workbook()
        ws = wb.add_sheet('CeoffMat')

        rowstart = 2
        colstart = 2
        num = 2
        for i in range(8*num):
            for k in range(8*num):
                ws.write(rowstart+i, colstart+k, 1)




        wb.save('mattest.xls')

a = lambdaunittest()
a.printMat()





























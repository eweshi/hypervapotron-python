#!/usr/bin/env python
from __future__ import division
from scipy import *
from math import *
from scipy.optimize import bisect
from sympy import sign
from numpy import linalg


def cot(x):
    return (1 / tan(x))


class DomainExpressions(object):
    """The Domain Expressions class contains the
    expressions of a domain for use in computation
    of temperature and heat flux distribution
    calculation. It also contains other important
    information about the methods."""


    # The eigen-values are stored in the
    #lambda lists

    lambdaList = []  #Stores a list of the lambda lists

    def __init__(self):
        self.tolerance = 0.3
        self.convergenceNum = 1
        self.thickness = 1.0
        self.width = 4
        self.width_c = 1
        self.height = 4
        self.height_c = 1

        self.Bi_1 = 1000
        self.Bi_2 = 1000
        self.Bi_3 = 1000


        self.k_3 = 1
        self.k_2 = 1
        self.k_3 = 1
        self.q_doubleprime_infinity = 1



        self.x_1 = (self.width / 2.0) - self.width_c
        self.H_1 = self.height - 2*(self.height_c) - self.thickness

        x = self.convergenceNum
        self.lambda1_1List = zeros(x)
        self.lambda1_2List = zeros(x)
        self.lambda2_1List = zeros(x)
        self.lambda2_2List = zeros(x)
        self.lambda2_3List = zeros(x)
        self.lambda3_1List = zeros(x)
        self.lambda3_2List = zeros(x)
        self.lambda4_1List = zeros(x)
        self.lambda4_2List = zeros(x)
        self.lambda5_1List = zeros(x)
        self.assignLambdaList()

        self.matrixDimensions = (8*self.convergenceNum, 8*self.convergenceNum)
        self.coeffMat = zeros(self.matrixDimensions)
        self.unknownCoefficients = zeros(x)
        self.constantVec = zeros(x)

        self.row = 0
        self.col = 0


    """This function assigns all of the lambdalists as elements to
    a universal lambda list"""

    def setBi_1(self, x):
        self.Bi_1 = x

    def setBi_2(self, x):
        self.Bi_2 = x

    def setBi_3(self, x):
        self.Bi_3 = x

    def updateH_1(self):
        self.H_1 = self.height - 2*(self.height_c) - self.thickness

    def setHeight(self, x):
        self.height = x
        self.updateH_1()

    def setHeightChannel(self, x):
        self.height_c = x
        self.updateH_1()

    def setThickness(self, x):
        self.thickness = x
        self.updateH_1()

    def setWidth(self, x):
        self.width = x
        self.updatex_1()

    def setWidthChannel(self, x):
        self.width_c = x
        self.updatex_1()

    def updatex_1(self):
        self.x_1 = (self.width/2.0) - self.width_c

    def assignLambdaList(self):
        #This is to avoid adding unwanted elements when
        #using the interactive enviorment
        del self.lambdaList[:]

        self.lambdaList.append(self.lambda1_1List)
        self.lambdaList.append(self.lambda1_2List)
        self.lambdaList.append(self.lambda2_1List)
        self.lambdaList.append(self.lambda2_2List)
        self.lambdaList.append(self.lambda2_3List)
        self.lambdaList.append(self.lambda3_1List)
        self.lambdaList.append(self.lambda3_2List)
        self.lambdaList.append(self.lambda4_1List)
        self.lambdaList.append(self.lambda4_2List)
        self.lambdaList.append(self.lambda5_1List)

    def setConvergeNum(self, x):
        """This function changes the dimensions of the lambda lists
        to accomodate the new convergence number"""


        print "Changing convergence!"
        self.convergenceNum = x
        self.lambda1_1List = zeros(x)
        self.lambda1_2List = zeros(x)
        self.lambda2_1List = zeros(x)
        self.lambda2_2List = zeros(x)
        self.lambda2_3List = zeros(x)
        self.lambda3_1List = zeros(x)
        self.lambda3_2List = zeros(x)
        self.lambda4_1List = zeros(x)
        self.lambda4_2List = zeros(x)
        self.lambda5_1List = zeros(x)
        self.assignLambdaList()

        self.matrixDimensions = (8*self.convergenceNum, 8*self.convergenceNum)
        self.coeffMat = zeros(self.matrixDimensions)
        self.unknownCoefficients = zeros(x)
        self.constantVec = zeros(x)

    def rootfind(self, func, list):
        """This function takes a function, computes every root.
        The number of roots it will obtain will be equal to the
        convergence Number. These roots are stored in the list
        that is supplied as an argument."""

        print "starting rootfind"
        index = 0
        a = 0.01
        b = a + self.tolerance
        i = 0

        while (i !=  self.convergenceNum):
            #Incrementation is done first
            #to avoid issues in ordering
            i += 1

            while sign(func(a)) == sign(func(b)):
                b += self.tolerance

            while not bisect(func, a, b):
                b += self.tolerance

            """if (i % 2) == 0:
                a = b
                continue"""

            #print brentq(func,a,b)
            list[index] = bisect(func, a, b)
            index += 1
            a = b

    def lam1_1Expression(self, x):
        """Defines The Eigen-value Expression for Domain 1_1 corresponding to
        lambda1_1. These eigen-values are found using root finding methods"""

        return cos(self.thickness * x) - ((sin(self.thickness*x)* self.thickness * x) / self.Bi_1)

    def lam1_2Expression(self, x):
        """Defines The Eigen-value Expression for Domain 1_2 corresponding to
        lambda1_2. These eigen-values are found by evaluating the simple expression
        where x is n = 1, 3, 5.. """

        return (x * pi /( 2 * self.width_c))

    def lam2_1Expression(self, x):
        """Defines The Eigen-value Expression for Domain 2_1 corresponding to
        lambda2_1. These eigen-values are found by evaluating the simple expression
        where x is n = 1, 3, 5.. """

        return (x * pi / (2 * self.thickness))

    def lam2_2Expression(self, x):
        """Defines The Eigen-value Expression for Domain 2_2 corresponding to
        lambda2_2. These eigen-values are found by evaluating the simple expression
        where x is n = 1, 3, 5.. """

        return (x * pi / (2 * self.x_1))

    def lam2_3Expression(self, x):
        """Defines The Eigen-value Expression for Domain 2_3 corresponding to
        lambda2_3. These eigen-values are found by evaluating the simple expression
        where x is n = 1, 3, 5.. Note: Lambda2_2 is equal in expression to lambda2_3"""

        return (x * pi / (2 * self.x_1))

    def lam3_1Expression(self, x):
        """Defines The Eigen-value Expression for Domain 3_1 corresponding to
        lambda3_1. These eigen-values are found using root finding methods """

        return (cos(x * self.x_1) - (sin(x*self.x_1)*x * self.x_1 / self.Bi_2))

    def lam3_2Expression(self, x):
        """Defines The Eigen-value Expression for Domain 3_2 corresponding to
        lambda3_2. These eigen-values are found using root finding methods"""

        return (cos(x * self.x_1) - sin(x*self.x_1)*(x * self.x_1 )/ self.Bi_2)

    def lam4_1Expression(self, x):
        """Defines The Eigen-value Expression for Domain 4_1 corresponding to
        lambda4_1. These eigen-values are found by evaluating the simple expression
        where x is n = 1, 3, 5.. """

        return ((x * pi) / (2 * self.x_1))

    def lam4_2Expression(self, x):
        """Defines The Eigen-value Expression for Domain 4_2 corresponding to
        lambda4_2. These eigen-values are found by evaluating the simple expression
        where x is n = 1, 2, 3, 4.. """

        return (x * pi / self.H_1)

    def lam5_1Expression(self, x):
        """Defines The Eigen-value Expression for Domain 5_1 corresponding to
        lambda5_1. These eigen-values are found by evaluating the simple expression
        where x is n = 1, 2, 3, 4.. """

        #This function needs to be verified.  Check that n is = 1,2,3
        # and not n = 1,3,5..
        return (cos(x * self.H_1) - (sin(x*self.H_1)*((x * self.H_1) * self.Bi_3)))

    def computelam1_1(self):
        """Populates the list with the associated eigen-values"""
        self.rootfind(self.lam1_1Expression, self.lambda1_1List)

    def computelam1_2(self):
        """Populates the list with the associated eigen-values"""
        n = 1
        for i in range(0, self.convergenceNum):
            self.lambda1_2List[i] = self.lam1_2Expression(n)
            n += 2

    def computelam2_1(self):
        """Populates the list with the associated eigen-values"""
        n = 1
        for i in range(0, self.convergenceNum):
            self.lambda2_1List[i] = self.lam2_1Expression(n)
            n += 2

    def computelam2_2(self):
        """Populates the list with the associated eigen-values"""
        n = 1
        for i in range(0, self.convergenceNum):
            self.lambda2_2List[i] = self.lam2_2Expression(n)
            n += 2

    def computelam2_3(self):
        """Populates the list with the associated eigen-values"""
        n = 1
        for i in range(0, self.convergenceNum):
            self.lambda2_3List[i] = self.lam2_3Expression(n)
            n += 2

    def computelam3_1(self):
        """Populates the list with the associated eigen-values"""
        self.rootfind(self.lam3_1Expression, self.lambda3_1List)

    def computelam3_2(self):
        """Populates the list with the associated eigen-values"""
        self.rootfind(self.lam3_2Expression, self.lambda3_2List)

    def computelam4_1(self):
        """Populates the list with the associated eigen-values"""
        n = 1
        for i in range(0, self.convergenceNum):
            self.lambda4_1List[i] = self.lam4_1Expression(n)
            n += 2

    def computelam4_2(self):
        """Populates the list with the associated eigen-values"""
        n = 1
        for i in range(0, self.convergenceNum):
            self.lambda4_2List[i] = self.lam4_2Expression(n)
            n += 1

    def computelam5_1(self):
        """Populates the list with the associated eigen-values"""\

        #This function needs to be verified.  Check that n is = 1,2,3
        # and not n = 1,3,5..
        self.rootfind(self.lam5_1Expression, self.lambda5_1List)

    def computelams(self):
        """Begins the computation of all of the eigen-values and places them into their
        respective lists. It computes the same number of eigen-values as the
        convergence number. that is # of eigenvalues = convergenceNum"""
        self.computelam1_1()
        self.computelam1_2()
        self.computelam2_1()
        self.computelam2_2()
        self.computelam2_3()
        self.computelam3_1()
        self.computelam3_2()
        self.computelam4_1()
        self.computelam4_2()
        self.computelam5_1()

    def beginMatrix(self):
        """We are solving a system of equations. The dimensions of our coefficient matrix
            is 8n x 8n where n is our chosen convergence number."""

        #Make sure dependent parameters are updated
        self.updateH_1()
        self.updatex_1()

        #Populate Coefficient Matrix
        self.populateSection1()
        self.populateSection2()
        self.populateSection3()
        self.populateSection4()
        self.populateSection5()
        self.populateSection6()
        self.populateSection7()
        self.populateSection8()


        #Populate the right hand side constant vector

        #Solve

    #The coefficient matrix will be divided into sections. Where section 1 corresponds to the
    #first domain with an unknown coefficient, section 2 to the second, etc
    

#Coefficient Expressions
    def d1_1coeffExpression(self, counter1, counter2):
        lam1 = self.lambda1_1List[counter1]
        lam2 = self.lambda2_1List[counter2]
        width = self.width
        width_c = self.width_c
        t = self.thickness

        a = ((exp(lam2*width_c) + exp(lam2*(width-width_c))))
        b = (lam2*sin(lam2*t)*cos(lam1*t)-lam1*cos(lam2*t)*sin(lam1*t))/((lam2**lam2)-(lam1**lam1))
        c = (exp(lam1*width_c)+ exp(-lam2*width_c))
        d = (2*lam1*t+sin(2*lam1*t))/(4*lam1)

        return (a*b)/(c*d)

    def d2_1coeffExpression(self, counter1, counter2):
        """This function returns the value evaluted with the derived coefficient expressions.
        This function belongs to domain 2_1 coefficient."""

        lam1 = self.lambda1_1List[counter2]
        lam2 = self.lambda2_1List[counter1]
        width = self.width
        width_c = self.width_c
        t = self.thickness

        a = (exp(lam1*width_c)+exp(-lam1*width_c))
        b = (lam2*sin(lam2*t)*cos(lam1*t) - lam1*cos(lam2*t)*sin(lam1*t))/((lam2**lam2)-(lam1**lam1))
        c = (exp(lam2*width_c)+exp(lam2*(width-width_c)))
        d = (2*lam2*t+sin(2*lam2*t))/(4*lam2)

        return (a*b)/(c*d)

    def d2_3_1coeffExpression(self, counter1, counter2):

        x_1 = self.x_1
        lam3 = self.lambda3_1List[counter2]
        lam2 = self.lambda2_3List[counter1]
        t = self.thickness
        H_c = self.height_c

        a = (exp(lam3*t)-exp(lam3*(2*t+2*H_c-t)))
        b = (lam3*sin(lam3*x_1)*cos(lam2*x_1)-lam2*cos(lam3*x_1)*sin(lam2*x_1))/(lam3**lam3-lam2**lam2)

        alpha = (1/(exp(lam2*t)+exp(-lam2*t)))
        sigma = alpha / ((2*lam2*x_1+sin(2*lam2*x_1))/(4*lam2))

        return sigma*(a*b)

    def d2_3_2coeffExpression(self, counter1, counter2):

        x_1 = self.x_1
        lam3 = self.lambda3_2List[counter2]
        lam2 = self.lambda2_3List[counter1]
        t = self.thickness
        H_c = self.height_c

        a = (exp(lam3*t)-exp(lam3*(2*t-t)))
        b = (lam3*sin(lam3*x_1)*cos(lam2*x_1)-lam2*cos(lam3*x_1)*sin(lam2*x_1))/(lam3**lam3-lam2**lam2)

        alpha = (1/(exp(lam2*t)+exp(-lam2*t)))
        sigma = alpha / ((2*lam2*x_1+sin(2*lam2*x_1))/(4*lam2))

        return sigma*(a*b)

    def d3_1_1coeffExpression(self, counter1, counter2):
        #Please check this expression to see if it matches with what I wrote.
        #Please check for subtleties
        #also check for naming consistency

        lam2 = self.lambda2_1List[counter2]
        lam3 = self.lambda3_1List[counter1]
        x_1 = self.x_1
        w = self.width
        H_c = self.height_c
        t = self.thickness

        var1 = exp(lam2*w/2) * (exp(-lam2*x_1)*((lam3*sin(lam3*x_1)-lam2*cos(lam3*x_1)+ lam2*exp(lam2*x_1))/(lam3**lam3+lam2**lam2)))
        var2 = exp(lam2*w/2) * ((exp(lam2*x_1) * (lam2*cos(lam3*x_1)+lam3*sin(lam3*x_1))/(lam2**lam2+lam3**lam3)) - (lam2)/(lam2**lam2+lam3**lam3))
        alpha = 1/(exp(lam3*t)-exp(lam3*(2*H_c+t)))
        top = cos(lam2*t)*alpha*(var1+var2)
        bottom = (2*lam3*x_1+sin(2*lam3*x_1))/(4*lam3)

        return top / bottom

    def d3_1_2coeffExpression(self, counter1, counter2):

        lam2 = self.lambda2_3List[counter2]
        lam3 = self.lambda3_1List[counter1]
        t = self.thickness
        x_1 = self.x_1
        H_c = self.height_c

        evar = (exp(lam3*t)+exp(-lam3*t))
        trigvar = ((lam3*sin(lam3*x_1)*cos(lam2*x_1)-lam2*cos(lam3*x_1)*sin(lam2*x_1)))/((lam3**lam3-lam2**lam2))
        cvar = evar*trigvar
        alpha = 1/(exp(lam3*t)-exp(lam3*(2*H_c+t)))
        top = alpha * cvar
        bottom = ((2*lam3*x_1+sin(2*lam3*x_1)))/((4*lam3))

        return top/bottom

    def d3_2_1coeffExpression(self, counter1, counter2):

        lam3 = self.lambda3_1List[counter1]
        lam4 = self.lambda4_1List[counter2]
        x_1 = self.x_1
        y_1 = self.H_1 # As in paper 3
        t = self.thickness
        H_c = self.height_c


        trigvar = ((lam4*sin(lam4*x_1)*cos(lam3*x_1)-lam3*cos(lam4*x_1)*sin(lam3*x_1)))/((lam4**lam4-lam3**lam3))
        top = cosh(lam4*y_1)*trigvar
        alpha = 1/(exp(lam3*(2*H_c+t))- exp(lam3*(t-2*H_c)))
        bottom = ((2*lam3*x_1+sin(2*lam3*x_1)))/((4*lam3))

        return ((top*alpha)/(bottom))

    def d3_2_2coeffExpression(self, counter1, counter2):

        lam3 = self.lambda3_1List[counter1]
        lam4 = self.lambda4_2List[counter2]
        x_1 = self.x_1
        y_1 = self.H_1 # As in paper 3
        t = self.thickness
        H_c = self.height_c

        trigvar = ((lam4*sinh(lam4*x_1)*cos(lam3*x_1)+lam3*cosh(lam4*x_1)*sin(lam3*x_1)))/((lam4**lam4+lam3**lam3))
        top = cos(lam4*y_1) * trigvar
        alpha = 1/(exp(lam3*(2*H_c+t))- exp(lam3*(t-2*H_c)))
        bottom = ((2*lam3*x_1+sin(2*lam3*x_1)))/((4*lam3))

        return (top*alpha)/(bottom)

    def d4_1_1coeffExpression(self, counter1, counter2):
        #TODO: YOU NEED TO ASK DR.BOYD WHAT YOU PLUG IN FOR THE VALUE Y HERE. CURRENTLY UNKNOWN
        #LOOK AT EXPRESSION DERIVATION SHEET

        lam3 = self.lambda3_1List[counter2]
        lam4 = self.lambda4_1List[counter1]
        x_1 = self.x_1
        H_c = self.height_c
        H_1 = self.H_1
        t = self.thickness

        y = 1 #TODO: FOR NOW Y IS 1

        evar = (exp(lam3*y)+exp(lam3*(2*t+2*H_c-y)))
        trigvar = ((lam4*sin(lam4*x_1)*cos(lam3*x_1)-lam3*cos(lam4*x_1)*sin(lam3*x_1)))/((lam4**lam4-lam3**lam3))
        top = (-lam3)*evar*trigvar
        bottom = cosh(lam4*H_1)* ((2*lam4*x_1+sin(2*lam4*x_1)))/((4*lam4))

        return top/bottom

    def d4_1_2coeffExpression(self, counter1, counter2):
        #TODO: LOOK AND SEE IF THE FRACTION FLIPPING BOTTOM THING IS CORRECT IN THE EXPRESSION SHEET
        lam3 = self.lambda3_1List[counter2]
        lam4 = self.lambda4_1List[counter1]
        x_1 = self.x_1
        H_c = self.height_c
        H_1 = self.H_1
        t = self.thickness
        y = 1 #TODO: FOR NOW Y IS 1

        evar = (exp(lam3*y)+exp(lam3*(2*t-y)))
        trigvar = ((lam4*sin(lam4*x_1)*cos(lam3*x_1)-lam3*cos(lam4*x_1)*sin(lam3*x_1)))/((lam4**lam4-lam3**lam3))
        top = (-lam3)*evar*trigvar
        bottom = cosh(lam4*H_1)* ((2*lam4*x_1+sin(2*lam4*x_1)))/((4*lam4))

        return top/bottom

    def d4_2_1coeffExpression(self, counter1, counter2):

        lam4 = self.lambda4_2List[counter1]
        lam5 = self.lambda5_1List[counter2]
        y_1 = self.H_1
        w_c = self.width_c
        x_1 = self.x_1

        trigvar = ((lam5*sin(lam5*y_1)*cos(lam4*y_1)-lam4*cos(lam5*y_1)*sin(lam4*y_1)))/((lam5**lam5-lam4**lam4))
        top = cosh(lam5*w_c) * trigvar
        bottom = cosh(lam4*x_1) * ((2*lam4*y_1+sin(2*lam4*y_1)))/((4*lam4))

        return top / bottom

    def d5_1_1coeffExpression(self, counter1, counter2):
        lam4 = self.lambda4_1List[counter2]
        lam5 = self.lambda5_1List[counter1]
        H_1 = self.H_1
        w_c = self.width_c
        x_1 = self.x_1

        k_v = self.k_3 #TODO: PLEASE CHECK THIS

        trigvar = ((lam5*sin(lam5*H_1)*cosh(lam4*H_1)+lam4*cos(lam5*H_1)*sin(lam4*H_1))/((lam4**lam4+lam5**lam5)))
        top = (-lam4)*sin(lam4*x_1)*trigvar
        alpha = (lam4/(lam5*k_v*sinh(lam5*w_c)))
        beta = ((2*lam5*H_1+sin(2*lam5*H_1)) / (4*lam5))

        return alpha*(1/beta)*top

    def d5_1_2coeffExpression(self, counter1, counter2):

        lam4 = self.lambda4_2List[counter2]
        lam5 = self.lambda5_1List[counter1]
        H_1 = self.H_1
        w_c = self.width_c
        x_1 = self.x_1

        k_v = self.k_3 #TODO: PLEASE CHECK THIS

        trigvar = ((lam5*sin(lam5*H_1)*cos(lam4*H_1)-lam4*cos(lam5*H_1)*sin(lam4*H_1))/((lam5**lam5-lam4**lam4)))
        top = (-lam4)*sinh(lam4*x_1) * trigvar

        alpha = (lam4/(lam5*k_v*sinh(lam5*w_c)))
        beta = ((2*lam5*H_1+sin(2*lam5*H_1)) / (4*lam5))

        return alpha*(1/beta)*top

#Independent "Constant" Expressions
#These constants do not form the right hand side of the system
#But they are included in other expressions within the system.

    def d1_2constExpression(self, counter):

        lam1 = self.lambda1_2List[counter]
        w_c = self.width_c
        t = self.thickness
        Bi = self.Bi_1
        Bi_wc_n = (1+Bi*(w_c/t)*(2/counter*pi))/(1+Bi*(w_c/t)*(2/counter*pi))
        T_ref = 1 # TODO: PLEASE CHECK WITH DR.BOYD WHAT THIS VALUE IS SUPPOSED TO BE.


        result = (( T_ref*(4*t/counter*pi) * sin(lam1*w_c) ) / (pi * counter) ) * pow((1-exp(2*lam1)*Bi_wc_n),-1)
        return result

    def d2_2constExpression(self, counter):

        lam2 = self.lambda2_2List[counter]
        t = self.thickness
        k_2 = self.k_2
        x_1 = self.x_1
        q = self.q_doubleprime_infinity


        a = -q*sin(lam2*x_1)*4*lam2
        b = (lam2**lam2) * k_2 * (1 + exp(2*t*lam2))
        c = (2*lam2*x_1 + sin(2*lam2*x_1))

        return a/(b*c)

#Matrix Constant Expressions
#TODO: For some reason all of these evaluate to zero.
    def d3_1_3constMatExpression(self, counter):

        return False


#Matrix Functions
    def populateSection1(self):
        """This function populates the first section of the matrix. The first section corresponds to the
        Domain1_1 coefficient. The loop indicies were determined by first generating a simple case of the matrix
        by hand, and noting where everything was. This was done for n = 2, 3, until a pattern could be deduced."""

        #Algorithm must start with row = 0 and col = 0
        self.row = 0
        self.col = 0
        assert self.row == 0
        assert self.col == 0
        counter1 = 0
        for i in range(self.row, self.convergenceNum):

            self.coeffMat[self.row, self.col] = 1

            counter2 = 0


            for k in range(self.convergenceNum, 2*self.convergenceNum):
                self.coeffMat[self.row, k] = self.d1_1coeffExpression(counter1, counter2)
                counter2 += 1

            counter1 += 1
            self.col += 1
            self.row += 1

    def populateSection2(self):
        """This function populates the second section of the Matrix. The second section
        Corresponds to the Domain 2_1 Coefficient"""

        counter1 = 0
        for i in range(self.row, 2*self.convergenceNum):
            self.coeffMat[self.row, self.col] = 1

            counter2 = 0
            for k in range(0, self.convergenceNum):
                self.coeffMat[self.row, k] = self.d2_1coeffExpression(counter1,counter2)
                counter2 += 1

            counter1 += 1
            self.col += 1
            self.row += 1

    def populateSection3(self):

        counter1 = 0
        for i in range(self.row, 3*self.convergenceNum):
            self.coeffMat[self.row, self.col] = 1

            counter2 = 0

            for k in range(self.convergenceNum*3, 4*self.convergenceNum):
                self.coeffMat[self.row, k] = self.d2_3_1coeffExpression(counter1, counter2)
                counter2 += 1

            counter2 = 0
            for k in range(self.convergenceNum*4, 5*self.convergenceNum):
                self.coeffMat[self.row, k] = self.d2_3_2coeffExpression(counter1, counter2)
                counter2 += 1
            counter1 += 1
            self.col += 1
            self.row += 1

    def populateSection4(self):

        counter1 = 0
        for i in range(self.row, 4*self.convergenceNum):
            self.coeffMat[self.row, self.col] = 1

            counter2 = 0
            for k in range(self.convergenceNum, 2*self.convergenceNum):
                self.coeffMat[self.row, k] = self.d3_1_1coeffExpression(counter1, counter2)
                counter2 += 1

            counter2 = 0
            for k in range(self.convergenceNum*2, 3*self.convergenceNum):
                self.coeffMat[self.row, k] = self.d3_1_2coeffExpression(counter1, counter2)
                counter2 += 1

            counter1 += 1
            self.col += 1
            self.row += 1

    def populateSection5(self):

        counter1 = 0
        for i in range(self.row, 5*self.convergenceNum):
            self.coeffMat[self.row, self.col] = 1

            counter2 = 0
            for k in range(self.convergenceNum*5, 6*self.convergenceNum):
                self.coeffMat[self.row, k] = self.d3_2_1coeffExpression(counter1, counter2)
                counter2 += 1

            counter2 = 0
            for k in range(self.convergenceNum*6, 7*self.convergenceNum):
                self.coeffMat[self.row, k] = self.d3_2_2coeffExpression(counter1, counter2)
                counter2 += 1

            counter1 += 1
            self.col += 1
            self.row += 1

    def populateSection6(self):

        counter1 = 0
        for i in range(self.row, 6*self.convergenceNum):
            self.coeffMat[self.row, self.col] = 1


            counter2 = 0
            for k in range(self.convergenceNum*3, 4*self.convergenceNum):
                self.coeffMat[self.row, k] = self.d4_1_1coeffExpression(counter1, counter2)
                counter2 += 1


            counter2 = 0
            for k in range(self.convergenceNum*4, 5*self.convergenceNum):
                self.coeffMat[self.row, k] = self.d4_1_2coeffExpression(counter1, counter2)
                counter2 += 1
            counter1 += 1
            self.col += 1
            self.row += 1

    def populateSection7(self):

        counter1 = 0
        for i in range(self.row, 7*self.convergenceNum):
            self.coeffMat[self.row, self.col] = 1


            counter2 = 0
            for k in range(self.convergenceNum*7, 8*self.convergenceNum):
                self.coeffMat[self.row, k] = self.d4_2_1coeffExpression(counter1, counter2)
                counter2 += 1

            counter1 += 1
            self.col += 1
            self.row += 1

    def populateSection8(self):

        counter1 = 0
        for i in range(self.row, 8*self.convergenceNum):
            self.coeffMat[self.row, self.col] = 1

            counter2 = 0
            for k in range(self.convergenceNum*5, 6*self.convergenceNum):
                self.coeffMat[self.row, k] = self.d5_1_1coeffExpression(counter1, counter2)
                counter2 += 1

            counter2 = 0
            for k in range(self.convergenceNum*6, 7*self.convergenceNum):
                self.coeffMat[self.row, k] = self.d5_1_2coeffExpression(counter1, counter2)
                counter2 += 1

            counter1 += 1
            self.col += 1
            self.row += 1

    def populateCoeffMat(self):
        """Function populates the coefficient Matrix. Please complete lambda computation first"""

        self.populateSection1()
        self.populateSection2()
        self.populateSection3()
        self.populateSection4()
        self.populateSection5()
        self.populateSection6()
        self.populateSection7()
        self.populateSection8()

    def solveSystem(self):

        #Solving the system Ax = b for the unknown coefficient vector x.
        #Where A is the coefficient matrix and b is the constant vector.


        return False

    def dummySolve(self):
        """Function performs the lambda computation and the coefficient matrix computation"""
        self.computelams()
        self.populateCoeffMat()
        self.unknownCoefficients = linalg.solve(self.coeffMat, self.constantVec)


    def compute_d1TempDist(self, x, y):
        #TODO: Ask Dr.Boyd how to account for the n = 1, 3, 5 in the series AND in the expression.
        computedSum = 0
        for i in range(self.convergenceNum):
            lam1 = self.lambda1_1List[i]
            computedSum += self.uCoeffList_d1_1[i] * (exp(lam1*x)+exp(-lam1*x)) * cos(lam1*y)

        for i in range(self.convergenceNum):
            #Compute Domain 1_2 or use it from somewhere else and add here.
            computedSum += 0

        return computedSum












